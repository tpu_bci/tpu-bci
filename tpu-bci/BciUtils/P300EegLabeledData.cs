﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BciUtils
{
    public class P300EegLabeledData : EegLabeledData
    {
        public int RowColumn { get; set; }

        public int LetterNumber { get; set; }
    }
}
