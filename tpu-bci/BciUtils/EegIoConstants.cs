﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BciUtils
{
    public class EegFileConstants
    {
        public const string InfoPostfix = "_info";
        public const string DataPostfix = "_data";
        public const string LabelsPostfix = "_classes";

        public const char ClassLabelSeparator = ':';
        public const char ValuesSeparator= ' ';
    }
}
