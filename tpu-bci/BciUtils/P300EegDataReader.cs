﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BciUtils
{
    public class P300EegDataReader
    {
        private short windowSize; //ms after onset
        private short samplingRate;

        public P300EegDataReader(short windowSize, short samplingRate)
        {
            this.windowSize = windowSize;
            this.samplingRate = samplingRate;
        }

        public List<P300EegLabeledData> ReadLabledData(string filename)
        {
            StreamReader labelsReader = new StreamReader(filename + EegFileConstants.LabelsPostfix);
            string dataFile = filename + EegFileConstants.DataPostfix;
            string[] lines = File.ReadAllLines(dataFile);

            List<P300EegLabeledData> labeledData = new List<P300EegLabeledData>();

            string classLabelString;
            short[] targets = null;
            int count = (int) Math.Round(samplingRate*windowSize/1000f);
            int letterNumber = 0;
            while ((classLabelString = labelsReader.ReadLine()) != null)
            {
                if (!classLabelString.StartsWith("Target"))
                {
                    if (targets == null)
                    {
                        throw new InvalidOperationException("Cannot find 'Target' key word");
                    }

                    string[] stringValues = classLabelString.Split(' ');
                    int sampleNumber = Int32.Parse(stringValues[0]);
                    short rowColumn = Int16.Parse(stringValues[1]);

                    List<float[]> values = ReadValues(lines, sampleNumber, sampleNumber + count);

                    P300EegLabeledData eeg = new P300EegLabeledData
                    {
                        Data = values,
                        ClassLabel = targets.Contains(rowColumn) ? 1 : 0,
                        SampleNumber = sampleNumber,
                        RowColumn = rowColumn,
                        LetterNumber = letterNumber
                    };

                    labeledData.Add(eeg);                
                }
                else
                {
                    string[] stringTargets = classLabelString.Split(' ');
                    targets = stringTargets.Skip(1).Select(o => Int16.Parse(o)).ToArray();
                    letterNumber++;
                }
            }

            return labeledData;
        }

        private List<float[]> ReadValues(string[] lines, int from, int to)
        {

            if (from > to)
            {
                throw new InvalidOperationException("Start indext exceeds end index");
            }

            List<float[]> data = new List<float[]>();
            for (int i = from; i <= to; i++)
            {
                string line = lines[i];

                if (i < from)
                {
                    continue;
                }

                string[] stringValues = line.Split(' ');
                float[] values = stringValues.Select(o => float.Parse(o)).ToArray();
                data.Add(values);
            }

            return data;

        }
    }
}
