﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BciUtils
{
    public class EegLabeledData
    {
        public List<float[]> Data { get; set; }
        public int ClassLabel { get; set; }
        public int SampleNumber { get; set; }
    }
}
