﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BciTest
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ITestable> tests = new List<ITestable>();
            tests.Add(new LDAClassifierTest());

            foreach (ITestable test in tests)
            {
                test.Test();
            }
        }
    }
}
