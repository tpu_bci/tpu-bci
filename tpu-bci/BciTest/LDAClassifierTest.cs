﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAnalysis.Classification;
using BciUtils;
using DataAnalysis;
using DataAnalysis.Filter;
using DataAnalysis.FeatureExtraction;

namespace BciTest
{
    class LDAClassifierTest : ITestable
    {
        private const string Filename = "Session 21.10.2015 01.16.07";
        private const int EpochTime = 10;
        private const int RestTime = 3;
        private const int SamplingRate = 1000;
        private static readonly string[] classes = new string[] { "LeftHand", "RightHand" };

        private const int WindowSize = 100;
        private const float Gap = 0.1f;

        private const float TrainingPart = 0.8f;

        public void Test()
        {
            MotorImageryEegDataReader dataReader = new MotorImageryEegDataReader(Filename, EpochTime, RestTime, SamplingRate, classes);
            List<EegLabeledData> data = dataReader.GetEegLabeledData(WindowSize, Gap);

            Console.WriteLine(data.Any(o=>o.ClassLabel != 0));

            EegLabeledDataSet eegLabeledDataSet = PrepareDataSet(data);

            MotorImageryFeatureExtraction featureExtractor = new MotorImageryFeatureExtraction(SamplingRate);
            List<double[]> trainingSamples = featureExtractor.Extract(eegLabeledDataSet.TrainingSet);
            List<double[]> testSamples = featureExtractor.Extract(eegLabeledDataSet.TestSet);

            LDAClassifier lda = new LDAClassifier();
            lda.Train(trainingSamples, eegLabeledDataSet.TrainingSet.Select(o => o.ClassLabel).ToList());

            double accuracy = GetAccuracy(lda, testSamples, eegLabeledDataSet.TestSet.Select(o => o.ClassLabel).ToList());

            Console.WriteLine("Accuracy = " + accuracy);

            Console.ReadKey();
        }

        private EegLabeledDataSet PrepareDataSet(List<EegLabeledData> data)
        {           
            List<EegLabeledData> trainingSet = new List<EegLabeledData>();
            List<EegLabeledData> testSet = new List<EegLabeledData>();
            Random random = new Random();

            foreach (EegLabeledData eeg in data)
            {
                double rand = random.NextDouble();
                if (rand > TrainingPart)
                {
                    testSet.Add(eeg);
                }
                else
                {
                    trainingSet.Add(eeg);
                }
            }

            EegLabeledDataSet eegLabeledDataSet = new EegLabeledDataSet
            {
                TrainingSet = trainingSet,
                TestSet = testSet
            };

            return eegLabeledDataSet;
        }

        private double GetAccuracy(IClassifier classifier, List<double[]> testSamples, List<int> outputs)
        {
            if (testSamples.Count != outputs.Count)
            {
                throw new ArgumentException("Number of samples must be equal to number of outputs");
            }

            int correctAnswers = 0;
            for (int i = 0; i < testSamples.Count; i++)
            {
                double[] sample = testSamples[i];
                double currectOutput = classifier.Classify(sample);
                if (currectOutput == outputs[i])
                {
                    correctAnswers++;
                }
            }

            return (double)correctAnswers / testSamples.Count;
        }

    }
}
