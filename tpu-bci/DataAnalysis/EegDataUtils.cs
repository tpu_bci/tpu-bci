﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis
{
    public class EegDataUtils
    {
        public static List<float[]> CovertToChannelsRepresentation(List<float[]> data)
        {
            List<float[]> channels = new List<float[]>();

            int channelCount = data[0].Length;

            for (int i = 0; i < channelCount; i++)
            {
                float[] channel = new float[data.Count];
                channels.Add(channel);
            }

            for (int i = 0; i < data.Count; i++)
            {
                float[] sample = data[i];

                for (int j = 0; j < sample.Length; j++)
                {
                    channels[j][i] = sample[j];
                }
            }

            return channels;
        }
    }
}
