﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BciUtils;

namespace DataAnalysis
{
    public class EegLabeledDataSet
    {
        public List<EegLabeledData> TrainingSet { get; set; }

        public List<EegLabeledData> TestSet { get; set; }
    }
}
