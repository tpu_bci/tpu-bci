﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis
{
    public class TrainingHelper
    {
        public static DataSet SplitData(List<double[]> data, List<int> labels, double ratio)
        {
            List<double[]> trainingData = new List<double[]>();
            List<int> trainingLabels = new List<int>();
            List<double[]> testData = new List<double[]>();
            List<int> testLabels = new List<int>();
            
            Random random = new Random();
            for (int i = 0; i < data.Count; i++)
            {
                double[] features = data[i];
                int label = labels[i];

                double rand = random.NextDouble();
                if (rand < ratio)
                {
                    trainingData.Add(features);
                    trainingLabels.Add(label);
                }
                else
                {
                    testData.Add(features);
                    testLabels.Add(label);
                }
            }

            DataSet dataSet = new DataSet
            {
                TrainingData = trainingData,
                TrainingLabels = trainingLabels,
                TestData = testData,
                TestLabels = testLabels
            };

            return dataSet;            
        }

        public static double CalculateAccuracy(List<int> answers, List<int> trueAnswers)
        {
            int correct = 0;
            for (int i = 0; i < answers.Count; i++)
            {
                if (answers[i] == trueAnswers[i])
                {
                    correct++;
                }
            }

            return (double)correct / answers.Count;
        }
    }
}
