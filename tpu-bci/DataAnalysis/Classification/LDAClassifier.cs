﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.Statistics.Analysis;
using BciUtils;

namespace DataAnalysis.Classification
{
    public class LDAClassifier : IClassifier
    {
        private LinearDiscriminantAnalysis lda;

        public void Train(List<double[]> data, List<int> outputs)
        {
            lda = new LinearDiscriminantAnalysis(data.ToArray(), outputs.ToArray());
            lda.Compute();
        }

        public double Classify(double[] data)
        {
            if (lda == null)
            {
                throw new Exception("Classifier is not initialized yet.");
            }

            double label = lda.Classify(data.Select(o=>(double)o).ToArray());

            return label;
        }
    }
}
