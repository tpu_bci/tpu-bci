﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.Math;

namespace DataAnalysis.Classification
{
    public abstract class AbstractEnsembleClassifier : IClassifier
    {
        private const float MinPartForEnsemble = 0.3f;

        private int ensambleSize;

        private List<IClassifier> classifiers;

        public AbstractEnsembleClassifier(int ensambleSize)
        {
            this.ensambleSize = ensambleSize;
        }

        public void Train(List<double[]> data, List<int> outputs)
        {
            classifiers = new List<IClassifier>();

            int dataSamplesPerEnsemble = (int)Math.Floor((float)data.Count / ensambleSize);

            for (int i = 0; i < ensambleSize; i++)
            {
                int startIndex = i*dataSamplesPerEnsemble;
                int endIndex = i - 1 < ensambleSize ? (i + 1) * dataSamplesPerEnsemble : data.Count - 1;

                if ((endIndex - startIndex) >= MinPartForEnsemble*dataSamplesPerEnsemble)
                {
                    List<double[]> dataBatch = data.GetRange(i*dataSamplesPerEnsemble, dataSamplesPerEnsemble);
                    List<int> outputBatch = outputs.GetRange(i*dataSamplesPerEnsemble, dataSamplesPerEnsemble);

                    IClassifier classifier = CreateClassifier();
                    classifier.Train(dataBatch, outputBatch);
                    classifiers.Add(classifier);
                }
            }
        }

        public double Classify(double[] data)
        {
            double averageScore = classifiers.Average(classifier => classifier.Classify(data));
            return averageScore;
        }

        public abstract IClassifier CreateClassifier();
    }
}
