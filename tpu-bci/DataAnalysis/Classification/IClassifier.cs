﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BciUtils;

namespace DataAnalysis.Classification
{
    public interface IClassifier
    {
        void Train(List<double[]> data, List<int> outputs);

        double Classify(double[] data);
    }
}
