﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis.Classification
{
    public class SVMEnsembleClassifier : AbstractEnsembleClassifier
    {

        public SVMEnsembleClassifier(int ensembleSize)
            : base(ensembleSize)
        {

        }

        public override IClassifier CreateClassifier()
        {
            return new SVMClassifier();
        }
    }
}
