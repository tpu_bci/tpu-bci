﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;

namespace DataAnalysis.Classification
{
    public class SVMClassifier : IClassifier
    {
        private SupportVectorMachine svm;

        public void Train(List<double[]> data, List<int> outputs)
        {
            int inputs = data[0].Length;
            svm = new SupportVectorMachine(inputs);

            if (outputs.Min() == 0)
            {
                outputs = ConvertLabels(outputs);
            }

            ISupportVectorMachineLearning teacher = new LinearNewtonMethod(svm, data.ToArray(), outputs.ToArray())
            {
                PositiveWeight = 0.5,
                NegativeWeight = 0.5,
                Complexity = 1.0
            };

            double error = teacher.Run();

           // SequentialMinimalOptimization smo = new SequentialMinimalOptimization(svm, data.ToArray(), outputs.ToArray());
         //   smo.Complexity = 1.0;
          //  double error = smo.Run();
        }

        public double Classify(double[] data)
        {
            if (svm == null)
            {
                throw new Exception("Classifier is not initialized yet.");
            }

            double score = svm.Compute(data.Select(o => (double)o).ToArray());

            return score;
        }

        private List<int> ConvertLabels(List<int> labels)
        {
            List<int> newLabels = new List<int>();
            foreach (int label in labels)
            {
                switch (label)
                {
                    case 0:
                        newLabels.Add(-1);
                        break;
                    case 1:
                        newLabels.Add(1);
                        break;
                    default:
                        throw new InvalidOperationException("Illegal calss label");
                }
            }

            return newLabels;
        }
    }
}
