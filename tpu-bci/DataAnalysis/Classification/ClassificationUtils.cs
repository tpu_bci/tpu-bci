﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis.Classification
{
    public class ClassificationUtils
    {
        public static List<double> ClassifyBatch(IClassifier classifier, List<double[]> data)
        {
            List<double> result = new List<double>();

            foreach (double[] sample in data)
            {
                double output = classifier.Classify(sample);
                result.Add(output);
            }

            return result;
        }

    }
}
