﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis.Classification
{
    public class LDAEnsembleClassifier : AbstractEnsembleClassifier
    {

        public LDAEnsembleClassifier(int ensembleSize)
            : base(ensembleSize)
        {

        }

        public override IClassifier CreateClassifier()
        {
            return new LDAClassifier();
        }
    }    
}
