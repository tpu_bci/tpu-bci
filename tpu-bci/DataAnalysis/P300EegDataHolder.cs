﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis
{
    public class P300EegDataHolder
    {
        private List<float[]> data = new List<float[]>();

        private Dictionary<int, int> sampleRowColumn = new Dictionary<int, int>(); //sample number, rowcolumn

        private int windowsSize;

        public P300EegDataHolder()
        {
            windowsSize = (int)Math.Round(P300Helper.DefaultSamplingRate * P300Helper.DefaultWindowsSize / 1000f);
        }

        public P300EegDataHolder(int windowSizeInMs, int samplingRate)
        {
            data = new List<float[]>();

            windowsSize = (int)Math.Round(samplingRate * windowSizeInMs / 1000f);
        }

        public void Push(List<float[]> data)
        {
            this.data.AddRange(data);
        }

        public void AddOnset(int rowColumn)
        {
            sampleRowColumn.Add(data.Count, rowColumn);
        }

        public List<List<float[]>> GetAveraged()
        {
            SortedDictionary<int, List<float[]>> averaged = new SortedDictionary<int, List<float[]>>();

            //if (sampleRowColumn.Count != 6*2*10)
            //{
            //    throw new InvalidOperationException("Wrong number of onsets");
            //}

            foreach (var kvp in sampleRowColumn)
            {
                List<float[]> currentData = data.Skip(kvp.Key).Take(windowsSize).ToList();
                if (averaged.ContainsKey(kvp.Value))
                {
                    List<float[]> storedData = averaged[kvp.Value];
                    for (int i = 0; i < currentData.Count; i++)
                    {
                        float[] values = currentData[i];
                        float[] storedValues = storedData[i];

                        for (int j = 0; j < values.Length; j++)
                        {
                            values[j] = values[j] + storedValues[j];
                        }
                    }
                }
                else
                {
                    averaged.Add(kvp.Value, currentData);
                }              
            }

            int count = sampleRowColumn.Values.Where(o => o == sampleRowColumn.Values.Take(1).Single()).Count();
            List<List<float[]>> average = averaged.Values.ToList();
            foreach (var rowcol in average)
            {
                foreach (float[] values in rowcol)
                {
                    for (int i=0; i<values.Length;i++)
                    {
                        values[i] = values[i] / count;
                    }
                }
            }

            return average;
        }

        public void Clear()
        {
            data.Clear();
            sampleRowColumn.Clear();
        }
    }
}
