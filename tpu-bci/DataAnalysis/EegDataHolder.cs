﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition
{
    public class EegDataHolder
    {
        private List<float[]> data;

        public EegDataHolder()
        {
            data = new List<float[]>();
        }

        public void Push(List<float[]> data)
        {
            this.data.AddRange(data);
        }

        public bool HasData(int windowSize)
        {
            return data.Count > windowSize;
        }

        public List<float[]> GetData(int windowSize)
        {
            List<float[]> windowedData = data.Take(windowSize).ToList();
            data.RemoveRange(0, windowSize);
            return windowedData;
        }
    }
}
