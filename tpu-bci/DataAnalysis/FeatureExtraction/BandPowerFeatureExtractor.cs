﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAnalysis.Filter;
using Accord.Audio;
using Accord.Controls;
using System.Threading;

namespace DataAnalysis
{
    public class BandPowerFeatureExtractor
    {
        private readonly int samplingRate;

        public BandPowerFeatureExtractor(int samplingRate)
        {
            this.samplingRate = samplingRate;
        }

        private static readonly List<float[]> PassFrequencies = new List<float[]>(
            new float[][]{ 
                new float[] { 8f,  12f},
                new float[] { 12f, 30f}
            });

        public double[] ExtractFeatures(List<float[]> windowedSignal)
        {
            double[] bandPowers = GetBandPowers(windowedSignal);
            return bandPowers;
        }

        public double[] GetBandPowers(List<float[]> signal)
        {
            double[] bandPowers = new double[PassFrequencies.Count * signal.Count];

            int bandPowerIndex = 0;
            BandPassFilter filter = new BandPassFilter();
            foreach (float[] samples in signal)
            {
                foreach (float[] frequencies in PassFrequencies)
                {   
                    float[] filtered = filter.Apply(samples, frequencies[0], frequencies[1], samplingRate);
                    bandPowers[bandPowerIndex++] = StatisticsUtil.GetAveragePower(filtered);
                }
            }

            return bandPowers;
        }       
    }
}
