﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BciUtils;

namespace DataAnalysis.FeatureExtraction
{
    public class MotorImageryFeatureExtraction
    {
        private int samplingRate;

        public MotorImageryFeatureExtraction(int samplingRate)
        {
            this.samplingRate = samplingRate;
        }

        public List<double[]> Extract(List<EegLabeledData> data)
        {
            BandPowerFeatureExtractor featureExtractor = new BandPowerFeatureExtractor(samplingRate);
            DataAnalysis.Filter.IFilter laplacian = new LaplacianMotorImageryFilter();
            List<double[]> trainingSamples = new List<double[]>();
            foreach (EegLabeledData eeg in data)
            {
                List<float[]> laplacianFiltered = laplacian.Filter(eeg.Data);
                List<float[]> transposed = Transpose(laplacianFiltered);
                double[] features = featureExtractor.ExtractFeatures(transposed);
                trainingSamples.Add(features);
            }
            return trainingSamples;
        }

        private List<float[]> Transpose(List<float[]> data)
        {
            List<float[]> transposed = new List<float[]>();

            for (int i = 0; i < data[0].Length; i++)
            {
                float[] signal = data.Select(o => o[i]).ToArray();
                transposed.Add(signal);
            }

            return transposed;
        }

    }
}
