﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;


namespace DataAnalysis.FeatureExtraction
{
    public class P300FeatureExtractor
    {
        private static readonly int[] Channels =
        {
            0,
            1,
            2,
            3,
            4,
            5, 
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16, 
            17, 
            18,
            19,
            20,
            21,
            22,
            23    
        };

        private const int DefaultFilterbanks = 20;

        private ushort filerbanks;

        public P300FeatureExtractor()
        {
            filerbanks = DefaultFilterbanks;
        }

        public float[] Extract(List<float[]> data)
        {
            int filterbankLength = (int)Math.Floor((float)data.Count / filerbanks);

            List<float[]> channels = EegDataUtils.CovertToChannelsRepresentation(data);

            float[] features = new float[Channels.Length * filerbanks];

            int chanelCount = 0;
            for (int channel = 0; channel < channels.Count; channel++)
            {
                if (Channels.Contains(channel))
                {
                    float[] channelData = channels[channel];

                    for (int i = 0; i < filerbanks; i++)
                    {
                        float[] filterbankData = new float[filterbankLength];
                        Array.Copy(channelData, i*filterbankLength, filterbankData, 0, filterbankLength);
                        float mean = filterbankData.Average();
                        features[chanelCount * filerbanks + i] = mean;
                    }

                    chanelCount++;
                }
            }

            return features;
        }
        
    }
}
