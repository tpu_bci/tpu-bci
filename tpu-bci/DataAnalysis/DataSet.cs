﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis
{
    public class DataSet
    {
        public List<double[]> TrainingData { get; set; }
        public List<int> TrainingLabels { get; set; }

        public List<double[]> TestData { get; set; }
        public List<int> TestLabels { get; set; }
    }
}
