﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis
{
    public class StatisticsUtil
    {
        public static float GetAveragePower(float[] data)
        {
            return (float)(data.Sum(o => o * o)) / data.Length;
        }

        public static float GetAverageAmplitude(float[] data)
        {
            return (float)(data.Sum(o => Math.Sqrt(o * o))) / data.Length;
        }    
    }
}
