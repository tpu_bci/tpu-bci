﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis.Filter
{
    public class VisualFilter
    {
        public float[] Filter(List<float[]> data)
        {
            float[] signal = new float[data.Count];

            for (int i = 0; i < data.Count; i++)
            {
                float[] sample = data[i];
                float derrivedSignal = sample[22] - sample[20]; //O2 - O1
                signal[i] = derrivedSignal;
            }

            return signal;
        }
    }
}
