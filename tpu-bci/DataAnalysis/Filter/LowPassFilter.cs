﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis.Filter
{
    class LowPassFilter : IPassFilter
    {

        public float[] ApplyPassFilter(float[] signal, float frequency, int samplingRate)
        {
            double rc = 1 / (2 * Math.PI * frequency);
            double dt = (double)1 / samplingRate; 

            double alpha = dt / (rc + dt);
            double[] filtered = new double[signal.Length];
            filtered[0] = signal[0];
            for (int i = 1; i < signal.Length; i++)
            {
                filtered[i] = alpha * signal[i] + (1 - alpha) * filtered[i - 1];
            }
            return filtered.Select(o=>(float)o).ToArray();
        }
    }
}
