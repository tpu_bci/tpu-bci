﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis.Filter
{
    interface IPassFilter
    {
        float[] ApplyPassFilter(float[] signal, float frequency, int samplingRate);
    }
}
