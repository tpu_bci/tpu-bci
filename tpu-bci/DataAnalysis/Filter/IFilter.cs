﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAnalysis.Filter
{
    public interface IFilter
    {
        List<float[]> Filter(List<float[]> signal); 
    }
}
