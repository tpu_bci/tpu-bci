﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.Audio;

namespace DataAnalysis.Filter
{
    public class BandPassFilter
    {
        public float[] Apply(float[] signal, float lowFrequency, float highFrequency, int samplingRate)
        {
            HighPassFilter highPassFilter = new HighPassFilter();
            float[] highPassed = highPassFilter.ApplyPassFilter(signal, lowFrequency, samplingRate);
            LowPassFilter lowPassFilter = new LowPassFilter();
            float[] lowPassed = lowPassFilter.ApplyPassFilter(highPassed, highFrequency, samplingRate);
            return lowPassed;
        }
    }
}
