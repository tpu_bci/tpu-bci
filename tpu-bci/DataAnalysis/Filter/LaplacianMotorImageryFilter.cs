﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAnalysis.Filter;

namespace DataAnalysis
{
    public class LaplacianMotorImageryFilter : IFilter
    {

        public List<float[]> Filter(List<float[]> signal)
        {
            List<float[]> filteredSignal = new List<float[]>();
            foreach (float[] sample in signal)
            {
                float[] filtered = new float[2];
                filtered[0] = 4 * sample[8] - sample[7] - sample[3] - sample[14] - sample[19];
                filtered[1] = 4 * sample[10] - sample[11] - sample[23] - sample[5] - sample[16];
                filteredSignal.Add(filtered);
            }

            return filteredSignal;
        }
    }
}
