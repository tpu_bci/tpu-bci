﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAcquisition;
using DataAcquisition.TransferObjects;
using DataAnalysis;
using DataAnalysis.Filter;

namespace tpu_bci
{
    public partial class MotorImageryClassificationForm : Form
    {
        private const int WindowSize = 200;

        private const float Threshold = 0.4f;

        //Eeg plotter
        private const int Duration = 10;

        private const int Scale = 100;
        //------

        private EegReceiver eegReceiver;

        private EegDataHolder eegDataHolder;

        private StatisticsUtil eegStatistics;

        private EegPlotter eegPlotter;

        public MotorImageryClassificationForm()
        {
            InitializeComponent();

            eegReceiver = new EegReceiver();
            eegReceiver.ProcessEegData = ProcessEegData;

            eegDataHolder = new EegDataHolder();

            eegStatistics = new StatisticsUtil();

            eegPlotter = new EegPlotter(eegPanel.CreateGraphics(), eegPanel.Width, eegPanel.Height);
            eegPlotter.Duration = Duration;
            eegPlotter.Scale = Scale;
        }

        private void ProcessEegData(EegSampleData eegSampleData)
        {
            eegDataHolder.Push(eegSampleData.Data);

            if (eegDataHolder.HasData(WindowSize))
            {
                List<float[]> data = eegDataHolder.GetData(WindowSize);

                IFilter filter = new LaplacianMotorImageryFilter();

                data = filter.Filter(data);

                this.Invoke((Action)delegate()
                {
                    EegSampleData newEegSampleData = new EegSampleData 
                    { 
                        Data = data,
                        EegChannelsNumber = data[0].Length,
                        SamplingRate = eegSampleData.SamplingRate
                    };
                    eegPlotter.Plot(newEegSampleData);
                });

                float[] c3Data = data.Select(o => o[0]).ToArray();
                float[] c4Data = data.Select(o => o[1]).ToArray();

                float c3Power = StatisticsUtil.GetAveragePower(c3Data);
                float c4Power = StatisticsUtil.GetAveragePower(c4Data);

                float threshold = Threshold*(c3Power + c4Power) / 2;

                this.Invoke((Action)delegate()
                {
                    this.c3ValueLabel.Text = c3Power.ToString("F0");
                    this.c4ValueLabel.Text = c4Power.ToString("F0");

                    if (c3Power - c4Power > threshold) //left
                    {
                        this.c3Label.ForeColor = Color.Red;
                        this.c4Label.ForeColor = Color.Black;
                    }
                    else if (c4Power - c3Power > threshold) //right
                    {
                        this.c3Label.ForeColor = Color.Black;
                        this.c4Label.ForeColor = Color.Red;
                    }
                    else
                    {
                        this.c3Label.ForeColor = Color.Black;
                        this.c4Label.ForeColor = Color.Black;
                    }
                });
            }            
        }     

        private void startButton_Click(object sender, EventArgs e)
        {
            eegReceiver.StartReceiving();      
        }

        private void MotorImageryClassificationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            eegReceiver.StopReceiving();
        }        
       
    }
}
