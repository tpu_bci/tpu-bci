﻿namespace tpu_bci
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.trainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingMotorImageryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classifyMotorImageryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vEPControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eEGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.p300ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordNewSessionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainClassifierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runTestSessionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainToolStripMenuItem,
            this.classificationToolStripMenuItem,
            this.eEGToolStripMenuItem,
            this.p300ToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.mainMenuStrip.Size = new System.Drawing.Size(400, 24);
            this.mainMenuStrip.TabIndex = 0;
            // 
            // trainToolStripMenuItem
            // 
            this.trainToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainingMotorImageryToolStripMenuItem});
            this.trainToolStripMenuItem.Name = "trainToolStripMenuItem";
            this.trainToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.trainToolStripMenuItem.Text = "Mental tasks";
            // 
            // trainingMotorImageryToolStripMenuItem
            // 
            this.trainingMotorImageryToolStripMenuItem.Name = "trainingMotorImageryToolStripMenuItem";
            this.trainingMotorImageryToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.trainingMotorImageryToolStripMenuItem.Text = "Record new session";
            this.trainingMotorImageryToolStripMenuItem.Click += new System.EventHandler(this.trainMotorImageryToolStripMenuItem_Click);
            // 
            // classificationToolStripMenuItem
            // 
            this.classificationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.classifyMotorImageryToolStripMenuItem,
            this.vEPControlToolStripMenuItem});
            this.classificationToolStripMenuItem.Name = "classificationToolStripMenuItem";
            this.classificationToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.classificationToolStripMenuItem.Text = "Classification";
            // 
            // classifyMotorImageryToolStripMenuItem
            // 
            this.classifyMotorImageryToolStripMenuItem.Name = "classifyMotorImageryToolStripMenuItem";
            this.classifyMotorImageryToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.classifyMotorImageryToolStripMenuItem.Text = "Motor Imagery";
            this.classifyMotorImageryToolStripMenuItem.Click += new System.EventHandler(this.classifyMotorImageryToolStripMenuItem_Click);
            // 
            // vEPControlToolStripMenuItem
            // 
            this.vEPControlToolStripMenuItem.Name = "vEPControlToolStripMenuItem";
            this.vEPControlToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.vEPControlToolStripMenuItem.Text = "VEP Control";
            this.vEPControlToolStripMenuItem.Click += new System.EventHandler(this.vEPControlToolStripMenuItem_Click);
            // 
            // eEGToolStripMenuItem
            // 
            this.eEGToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plotToolStripMenuItem});
            this.eEGToolStripMenuItem.Name = "eEGToolStripMenuItem";
            this.eEGToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.eEGToolStripMenuItem.Text = "Raw EEG";
            // 
            // plotToolStripMenuItem
            // 
            this.plotToolStripMenuItem.Name = "plotToolStripMenuItem";
            this.plotToolStripMenuItem.Size = new System.Drawing.Size(95, 22);
            this.plotToolStripMenuItem.Text = "Plot";
            this.plotToolStripMenuItem.Click += new System.EventHandler(this.plotToolStripMenuItem_Click);
            // 
            // p300ToolStripMenuItem
            // 
            this.p300ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recordNewSessionToolStripMenuItem,
            this.trainClassifierToolStripMenuItem,
            this.runTestSessionToolStripMenuItem});
            this.p300ToolStripMenuItem.Name = "p300ToolStripMenuItem";
            this.p300ToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.p300ToolStripMenuItem.Text = "P300";
            // 
            // recordNewSessionToolStripMenuItem
            // 
            this.recordNewSessionToolStripMenuItem.Name = "recordNewSessionToolStripMenuItem";
            this.recordNewSessionToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.recordNewSessionToolStripMenuItem.Text = "Record new session";
            this.recordNewSessionToolStripMenuItem.Click += new System.EventHandler(this.recordNewSessionToolStripMenuItem_Click);
            // 
            // trainClassifierToolStripMenuItem
            // 
            this.trainClassifierToolStripMenuItem.Name = "trainClassifierToolStripMenuItem";
            this.trainClassifierToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.trainClassifierToolStripMenuItem.Text = "Train classifier";
            this.trainClassifierToolStripMenuItem.Click += new System.EventHandler(this.trainClassifierToolStripMenuItem_Click);
            // 
            // runTestSessionToolStripMenuItem
            // 
            this.runTestSessionToolStripMenuItem.Name = "runTestSessionToolStripMenuItem";
            this.runTestSessionToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.runTestSessionToolStripMenuItem.Text = "Run test session";
            this.runTestSessionToolStripMenuItem.Click += new System.EventHandler(this.runTestSessionToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 346);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TPU BCI";
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem trainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainingMotorImageryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classifyMotorImageryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eEGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vEPControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem p300ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordNewSessionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainClassifierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runTestSessionToolStripMenuItem;
    }
}