﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using BciUtils;
using DataAcquisition;
using DataAcquisition.TransferObjects;
using DataAnalysis;
using DataAnalysis.Classification;
using DataAnalysis.FeatureExtraction;

namespace tpu_bci
{
    public partial class P300Form : Form
    {
        //Displaying letters - constants
        private const string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789_";

        private const string TargetWord = "BRAIN_COMPUTER_INTERFACE"; //_IN_TOMSK_POLYTECHNIC_UNIVERSITY

        private const int SamplingRate = P300Helper.DefaultSamplingRate;

        private const int HeaderHeight = 60;

        private const int HeaderRightMargin = 30;

        private const int MatrixSize = 6;

        private const int FontSize = 48;

        private const int FlashDuration = 100; //ms

        private const int Delay = 75; //ms

        private const int Pause = 1500; //ms

        private const int DefaultTrialCount = 15; 

        //Displaying letters
        private readonly Brush backgroundBrush = Brushes.Black;

        private readonly Brush highlightedBrush = Brushes.White;

        private readonly Brush unhighlightedBrush = Brushes.DimGray;

        private float horizontalStep;

        private float verticalStep;

        private Graphics graphics;

        //Eeg recording
        private Thread guiControThread;

        private IEegReceiver eegReceiver;

        private EegDataWriter writer;

        private volatile bool isStarted;

        private volatile bool isNewRowColumn;
        
        // TODO: possibly need to use syncronization to update two fields at once//
        private volatile bool isNewLetter; ///////////////////////////////////////

        private volatile char currentLetter;

        private readonly int trialCount;

        private IClassifier classifier;

        private P300EegDataHolder dataHolder;

        private Random random = new Random();

        private StringBuilder header = new StringBuilder();

        private SortedList<long, int> onsets = new SortedList<long, int>();

        private Object onsetLock = new Object();

        private Object dataLock = new Object();


        public P300Form(int trialCount = DefaultTrialCount, IClassifier classifier = null)
        {
            InitializeComponent();

            this.trialCount = trialCount;

            this.classifier = classifier;

            dataHolder = new P300EegDataHolder();

            eegReceiver = new EegReceiver { ProcessEegData = ProcessEegData };          
        }

        private void P300Form_Paint(object sender, PaintEventArgs e)
        {
            horizontalStep = (float)this.Width / (MatrixSize + 1);
            verticalStep = (float)(this.Height - HeaderHeight) / (MatrixSize + 1);

            graphics = this.CreateGraphics();

            DrawLetters();
        }

        //TODO: use P300EegDataHolder here
        private void ProcessEegData(EegSampleData eegSampleData)
        {
            if (!isStarted)
            {
                return;
            }

            if (isNewLetter && classifier == null)
            {
                int currentLetterNumber = Letters.IndexOf(currentLetter) + 1;

                int row = (currentLetterNumber - 1) / MatrixSize + 1;

                int column = currentLetterNumber % MatrixSize != 0
                    ? currentLetterNumber % MatrixSize : MatrixSize;

                writer.WriteTargetRowColumn(row, column + MatrixSize);        

                isNewLetter = false;
            }

            if (isNewRowColumn)
            {
                // Как пришел сампл расчитать время первого сэмпла учитывая количиство
                // и частоту дискртизации и сравнить их поочереди (а лучше записывать в буфер пока нет атаки)
                // c записанными началами атак, затем произвести запись (возможно пачками)

                //TODO: UI lags - problems with locking...
                SortedList<long, int> onsetList;
                lock (onsetLock)
                {
                    onsetList = new SortedList<long, int>(onsets);
                    onsets.Clear();                   
                }

                int onsetPosition = 0;
                long dataTimestamp = eegSampleData.Timestamp;
                double timePerSample = Math.Round(1000f / SamplingRate);
                List<float[]> buffer = new List<float[]>();
                for (int i = 0; i < eegSampleData.Data.Count; i++)
                {
                    float[] currentData = eegSampleData.Data[i];
                    double currentTimestamp = dataTimestamp + i * timePerSample;

                    bool hasOnset = onsetPosition < onsetList.Count;
                    long onsetTimestamp = -1;
                    if (hasOnset)
                    {
                        onsetTimestamp = onsetList.Keys[onsetPosition];
                    }

                    if (hasOnset && currentTimestamp >= onsetTimestamp)
                    {
                        lock (dataLock)
                        {
                            dataHolder.Push(buffer);
                        }

                        int rowColumn = onsetList[onsetTimestamp];

                        dataHolder.AddOnset(rowColumn);                       

                        if (classifier == null)
                        {                           
                            writer.WriteSamples(buffer);
                            writer.WriteFlashOnset(rowColumn);
                        }

                        buffer.Clear();
                        buffer.Add(currentData);

                        onsetPosition++;
                    }
                    else
                    {
                        buffer.Add(currentData);
                    }

                }

                if (onsetPosition < onsetList.Count)
                {
                    //TODO: UI lags - problems with locking...
                    lock (onsetLock)
                    {
                        for (int i = onsetPosition; i < onsetList.Count; i++)
                        {
                            long time = onsetList.Keys[i];
                            //TODO: is it critical withot syncronization?
                            onsets.Add(time, onsetList[time]);
                        }

                        isNewRowColumn = false;
                    }
                }

                if (buffer.Count > 0)
                {

                    lock (dataLock)
                    {
                        dataHolder.Push(buffer);
                    }
                    
                    if (classifier == null)
                    {
                        writer.WriteSamples(buffer);
                    }
                }
            }
            else
            {
                lock (dataLock)
                {
                    dataHolder.Push(eegSampleData.Data);
                }

                if (classifier == null)
                {
                    writer.WriteSamples(eegSampleData.Data);
                }
            }

            isNewRowColumn = false;
        }


        private void DrawLetters()
        {
            for (int row = 1; row <= MatrixSize; row++)
            {
                DrawRow(row, unhighlightedBrush);
            }
        }

        private void DrawRow(int row, Brush brush)
        {
            for (int column = 1; column <= MatrixSize; column++)
            {
                DrawLetter(row, column, brush);
            }
        }

        private void DrawColumn(int column, Brush brush)
        {
            for (int row = 1; row <= MatrixSize; row++)
            {
                DrawLetter(row, column, brush);
            }
        }

        private void DrawLetter(int row, int column, Brush brush)
        {
            char letter = Letters[(row - 1) * MatrixSize + column - 1];

            Font font = new Font(FontFamily.GenericSansSerif, FontSize, FontStyle.Bold);
             
            float x = column * horizontalStep - (float)font.Height / 2;
            float y = row * verticalStep - (float)font.Height / 2 + HeaderHeight;
            graphics.DrawString(letter.ToString(), font, brush, x, y);
        }

        private void DrawHeader(string text, Brush brush)
        {
            if (header.Length > 0)
            {
                graphics.FillRectangle(backgroundBrush, 0, 0, this.Width, HeaderHeight * 2);                
            }

            header.Append(text);

            Font font = new Font(FontFamily.GenericSansSerif, FontSize, FontStyle.Bold);

            float x = (float) (this.Width - font.Height*header.Length / 2) / 2;
            float y = (float)(HeaderHeight) / 2;
            graphics.DrawString(header.ToString(), font, brush, x, y);
        }

        private void ClearHeader()
        {
            graphics.FillRectangle(backgroundBrush, 0, 0, this.Width, HeaderHeight*2);
            header.Clear();
        }

        private void P300Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (isStarted)
                {
                    isStarted = false;

                    if (classifier == null)
                    {
                        writer.Close();
                    }
                }
                else
                {                   
                    this.Close();
                }
            }
        }

        private void P300Form_MouseClick(object sender, MouseEventArgs e)
        {
            if (isStarted)
            {
                return;
            }

            Start();
        }

        private void Start()
        {
            try
            {
                eegReceiver.StartReceiving();
            }
            catch (Exception)
            {
                MessageBox.Show("Аппаратные проблемы с устройством ЭЭГ");
                return;

               // eegReceiver = new FakeEegReceiver { ProcessEegData = ProcessEegData };
              //  eegReceiver.StartReceiving();
            }

            if (classifier == null)
            {
                string fileName = "Session " + DateTime.Now.ToString("dd.MM.yyyy hh.mm.ss");
                writer = new EegDataWriter(fileName);

                BasicInfo basicInfo = eegReceiver.BasicInfo;
                if (basicInfo != null)
                {
                    writer.WriteInfo(
                        basicInfo.EegChannelsNumber,
                        basicInfo.EventChannelsNumber,
                        basicInfo.SamplingRate
                    );
                }
            }

            isStarted = true;

            guiControThread = new Thread(RunGui);
            guiControThread.IsBackground = true;
            guiControThread.Start();
        }

        private void RunGui()
        {           
            int letter = 0;
            bool stop = false;
            bool firstRun = true;
            while (!stop && isStarted)
            {
                currentLetter = TargetWord[letter++];
                string header = classifier != null ? "Get ready..." : currentLetter.ToString();

                if (firstRun || classifier == null)
                {
                    ExecuteInGuiThread((Action)delegate()
                    {
                        DrawHeader(header, highlightedBrush);
                        Thread.Sleep(2 * Pause);
                        ClearHeader();
                        Thread.Sleep(Pause);
                    });
                }
                else
                {
                    Thread.Sleep(Pause);
                }

                firstRun = false;           

                isNewLetter = true;

                RunTrial();

                while (isNewRowColumn)
                {
                    //So just wating for data for flashes
                }

                if (classifier != null)
                {                   
                    Classify();
                }

                dataHolder.Clear();
               

                stop = classifier == null && letter >= TargetWord.Length;
            }           

            isStarted = false;
        }

        private void Classify()
        {

            List<List<float[]>> averaged = null;

            lock (dataLock)
            {
                averaged = dataHolder.GetAveraged();
            }

           if (averaged.Count != MatrixSize * 2)
           {
               MessageBox.Show("Сигнал не соответсвет размеру матрицы букв");
           }

           P300FeatureExtractor featureExtractor = new P300FeatureExtractor();
           double[] rows = new double[MatrixSize];
           double[] columns = new double[MatrixSize];
           for (int i = 0; i < averaged.Count; i++)
           {
               List<float[]> signal = averaged[i];
               float[] features = featureExtractor.Extract(signal);
               double[] doubleValues = features.Select(o => (double)o).ToArray();
               
               double result = classifier.Classify(doubleValues);
               if (i < MatrixSize)
               {
                   rows[i] = result;
               }
               else
               {
                   columns[i - MatrixSize] = result;
               }                   
           }

           //TODO: what we will do if several maxunum elements?
           int row = Array.IndexOf(rows, rows.Max());
           int column = Array.IndexOf(columns, columns.Max());
           char letter = Letters[row + column * MatrixSize];

           ExecuteInGuiThread((Action)delegate()
           {
               DrawHeader(letter.ToString(), highlightedBrush);
           });          
        }

        private void RunTrial()
        {
            for (int trial = 1; trial <= trialCount; trial++)
            {
                List<int> rowColumns = new List<int>(new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });

                while (rowColumns.Any())
                {
                    int rand = random.Next(rowColumns.Count);
                    int rowColumn = rowColumns[rand];

                    rowColumns.RemoveAt(rand);

                    ExecuteInGuiThread((Action)delegate()
                    {
                        if (rowColumn > MatrixSize)
                        {
                            DrawRow(rowColumn - MatrixSize, highlightedBrush);
                        }
                        else
                        {
                            DrawColumn(rowColumn, highlightedBrush);
                        }

                    });

                    long timestamp = TimestampHelper.GetInstance().GetCurrentMilliseconds();

                    //I think we can do it asynchronously 
                    lock (onsetLock)
                    {
                        isNewRowColumn = true;
                        onsets.Add(timestamp, rowColumn);
                    }                 
                                      

                    Thread.Sleep(FlashDuration);

                    ExecuteInGuiThread((Action)delegate()
                    {
                        if (rowColumn > MatrixSize)
                        {
                            DrawRow(rowColumn - MatrixSize, unhighlightedBrush);
                        }
                        else
                        {
                            DrawColumn(rowColumn, unhighlightedBrush);
                        }
                    });

                    Thread.Sleep(Delay);
                }

                Thread.Sleep(Pause);
            }
        }

        private void ExecuteInGuiThread(Delegate method)
        {
            if (isStarted)
            {
                this.Invoke(method);
            }
        }
    }
}
