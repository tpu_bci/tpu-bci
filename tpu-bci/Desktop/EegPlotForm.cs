﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAcquisition;
using System.Threading;
using DataAcquisition.TransferObjects;

namespace tpu_bci
{
    public partial class EegPlotForm : Form
    {
        public static readonly string ServerName = Connection.GetServerName();
        public static readonly int PortNumber = Connection.GetPortNumber();

        private TcpAcquirer tcpAcquirer;

        private EegPlotter eegPlotter;

        private Thread plotEegDataThread;

        public EegPlotForm()
        {
            InitializeComponent();

            Graphics graphics = this.eegDataPanel.CreateGraphics();
            eegPlotter = new EegPlotter(graphics, this.eegDataPanel.Width, eegDataPanel.Height);
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            IDataReceiver dataReceiver = new DataReceiver(this);
            tcpAcquirer = new TcpAcquirer(dataReceiver);
            tcpAcquirer.Start(ServerName, PortNumber);

            plotEegDataThread = new Thread(new ThreadStart(ProcessEeg));
            plotEegDataThread.Start();

            eegPlotter.Scale = (int) this.scaleNumericUpDown.Value;
            eegPlotter.Duration = (int) this.durationNumericUpDown.Value;

            Started(true);        
        }
         
        private void ProcessEeg()
        {
            while (tcpAcquirer.IsActive)
            {
                if (tcpAcquirer.HasEegData)
                {
                    EegSampleData sampleData = tcpAcquirer.GetEegData();
                    DrawEeg(sampleData);
                }
                else
                {
                    Thread.Sleep(20); //wait for sampleData
                }
            }
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            tcpAcquirer.Stop();
            Started(false);            
        }

        private void Started(bool isStarted)
        {
            this.startButton.Enabled = !isStarted;
            this.stopButton.Enabled = isStarted;
        }

        private void DrawEeg(EegSampleData eegSampleData)
        {
            this.eegDataPanel.Invoke((Action)delegate()
            {
                eegPlotter.Plot(eegSampleData);
            });
        }

       class DataReceiver : IDataReceiver
       {
           private readonly EegPlotForm mainForm;

           public DataReceiver(EegPlotForm form)
           {
               mainForm = form;
           }

            public void ProcessBasicInfo(BasicInfo basicInfo)
            {
                string info = string.Empty;
                info += "EEG:";
                info += basicInfo.EegChannelsNumber;
                info += "+";
                info += basicInfo.EventChannelsNumber;
                info += ";block size:";
                info += basicInfo.SampelsPerBlock;
                info += ";rate:";
                info += basicInfo.SamplingRate;
                info += "Hz;bits:";
                info += basicInfo.DataSize * 8;
                info += ";res:";
                info += basicInfo.Resolution.ToString("F3");

                mainForm.statusTextBox.Invoke((Action)delegate()
                {
                    mainForm.statusTextBox.Text = info;
                });
            }

            public bool HasEegData()
            {
                throw new NotImplementedException();
            }

            public byte[] GetEegData()
            {
                throw new NotImplementedException();
            }

            public void StartAcquisition()
            {
               
            }

            public void StopAcquisition()
            {
                
            }

            public void StartImpedance()
            {
                
            }

            public void Disconnected()
            {
                
            }
        }

       private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
       {
           if (tcpAcquirer != null)
           {
               tcpAcquirer.Stop();
           }
       }

       private void motorImageryToolStripMenuItem_Click(object sender, EventArgs e)
       {
           plotEegDataThread.Interrupt();
       }      
    }
}
