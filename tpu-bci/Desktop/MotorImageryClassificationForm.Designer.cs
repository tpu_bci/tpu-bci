﻿namespace tpu_bci
{
    partial class MotorImageryClassificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startButton = new System.Windows.Forms.Button();
            this.c3Label = new System.Windows.Forms.Label();
            this.c4Label = new System.Windows.Forms.Label();
            this.c3ValueLabel = new System.Windows.Forms.Label();
            this.c4ValueLabel = new System.Windows.Forms.Label();
            this.eegPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(1, 3);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(83, 27);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // c3Label
            // 
            this.c3Label.AutoSize = true;
            this.c3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.c3Label.Location = new System.Drawing.Point(129, 94);
            this.c3Label.Name = "c3Label";
            this.c3Label.Size = new System.Drawing.Size(64, 36);
            this.c3Label.TabIndex = 1;
            this.c3Label.Text = "Left";
            // 
            // c4Label
            // 
            this.c4Label.AutoSize = true;
            this.c4Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.c4Label.Location = new System.Drawing.Point(354, 94);
            this.c4Label.Name = "c4Label";
            this.c4Label.Size = new System.Drawing.Size(86, 36);
            this.c4Label.TabIndex = 2;
            this.c4Label.Text = "Right";
            // 
            // c3ValueLabel
            // 
            this.c3ValueLabel.AutoSize = true;
            this.c3ValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.c3ValueLabel.Location = new System.Drawing.Point(93, 177);
            this.c3ValueLabel.Name = "c3ValueLabel";
            this.c3ValueLabel.Size = new System.Drawing.Size(137, 36);
            this.c3ValueLabel.TabIndex = 3;
            this.c3ValueLabel.Text = "C3Value";
            // 
            // c4ValueLabel
            // 
            this.c4ValueLabel.AutoSize = true;
            this.c4ValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.c4ValueLabel.Location = new System.Drawing.Point(328, 177);
            this.c4ValueLabel.Name = "c4ValueLabel";
            this.c4ValueLabel.Size = new System.Drawing.Size(137, 36);
            this.c4ValueLabel.TabIndex = 4;
            this.c4ValueLabel.Text = "C4Value";
            // 
            // eegPanel
            // 
            this.eegPanel.BackColor = System.Drawing.SystemColors.Window;
            this.eegPanel.Location = new System.Drawing.Point(1, 297);
            this.eegPanel.Name = "eegPanel";
            this.eegPanel.Size = new System.Drawing.Size(549, 189);
            this.eegPanel.TabIndex = 5;
            // 
            // MotorImageryClassificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 486);
            this.Controls.Add(this.eegPanel);
            this.Controls.Add(this.c4ValueLabel);
            this.Controls.Add(this.c3ValueLabel);
            this.Controls.Add(this.c4Label);
            this.Controls.Add(this.c3Label);
            this.Controls.Add(this.startButton);
            this.Name = "MotorImageryClassificationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Motor Imagery Classification";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MotorImageryClassificationForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label c3Label;
        private System.Windows.Forms.Label c4Label;
        private System.Windows.Forms.Label c3ValueLabel;
        private System.Windows.Forms.Label c4ValueLabel;
        private System.Windows.Forms.Panel eegPanel;
    }
}