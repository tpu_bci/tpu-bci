﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BciUtils;
using DataAnalysis;
using DataAnalysis.Classification;
using DataAnalysis.FeatureExtraction;

namespace tpu_bci
{
    public partial class ClassifierTrainingForm : Form
    {
        private const string SVM = "SVM";

        private const string LDA = "LDA";

        private const int MaxEnsembleSize = 20;

        private const short WindowSize = (short)P300Helper.DefaultWindowsSize; //ms

        private const short SamplingRate = (short)P300Helper.DefaultSamplingRate; //Hz

        private const double CrossValidation = 0.9;

        public IClassifier Classifier { get; set; }

        public ClassifierTrainingForm()
        {
            InitializeComponent();

            this.typeComboBox.Items.Add(SVM);
            this.typeComboBox.Items.Add(LDA);
            this.typeComboBox.SelectedItem = SVM;

            for (int i = 1; i <= MaxEnsembleSize; i++)
            {
                this.EnsembleSizeComboBox.Items.Add(i);
            }
            EnsembleSizeComboBox.SelectedIndex = 0;
        }

        private void trainButton_Click(object sender, EventArgs e)
        {           
            if (string.IsNullOrEmpty(this.pathTextBox.Text))
            {
                MessageBox.Show("Сначала выберите файл");
                return;
            }

            string oringFilename = openFileDialog.FileName;
            string filename = oringFilename.Substring(0, oringFilename.Length - EegFileConstants.DataPostfix.Length);

            P300EegDataReader reader = new P300EegDataReader(WindowSize, SamplingRate);
            List<P300EegLabeledData> labeledData = reader.ReadLabledData(filename);

            //TODO: group by letters and then extract features
            P300FeatureExtractor featureExtractor = new P300FeatureExtractor();
            List<double[]> data = new List<double[]>();
            foreach (P300EegLabeledData eeg in labeledData)
            {
                float[] features = featureExtractor.Extract(eeg.Data);
                double[] doubleValues = features.Select(o => (double)o).ToArray();
                data.Add(doubleValues);
            }
            List<int> labels = labeledData.Select(o=>o.ClassLabel).ToList();

            //TODO: need to split by letters with signal averaging
            DataAnalysis.DataSet dataSet = TrainingHelper.SplitData(data, labels, CrossValidation);

            int ensembleSize = Int32.Parse(this.EnsembleSizeComboBox.Text);
            Classifier = CreateClassifier(this.typeComboBox.Text, ensembleSize);
            Classifier.Train(dataSet.TrainingData, dataSet.TrainingLabels);

            MessageBox.Show("Выполнено");

            this.DialogResult = DialogResult.OK;

            this.Close();

          //  double accuracy = TrainingHelper
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Data file (*_data)|*" + EegFileConstants.DataPostfix;
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.pathTextBox.Text = openFileDialog.FileName;
            }
        }

        private IClassifier CreateClassifier(string type, int ensembleSize)
        {
            IClassifier classifier = null;
            if (ensembleSize <= 1)
            {
                switch (type)
                {
                    case SVM:
                        classifier = new SVMClassifier();
                        break;
                    case LDA:
                        classifier = new LDAClassifier();
                        break;
                    default:
                        throw new ArgumentException("Unknown classifier type");
                }
            }
            else
            {
                switch (type)
                {
                    case SVM:
                        classifier = new SVMEnsembleClassifier(ensembleSize);
                        break;
                    case LDA:
                        classifier = new LDAEnsembleClassifier(ensembleSize);
                        break;
                    default:
                        throw new ArgumentException("Unknown classifier type");
                }
            }

            return classifier;
        }
    }
}
