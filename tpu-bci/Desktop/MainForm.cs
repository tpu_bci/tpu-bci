﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAnalysis.Classification;

namespace tpu_bci
{
    public partial class MainForm : Form
    {
        private const int TrialCount = 10;

        private IClassifier p300Classifier = null;

        public MainForm()
        {
            InitializeComponent();
        }

        private void classifyMotorImageryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MotorImageryClassificationForm form = new MotorImageryClassificationForm();
            form.ShowDialog();
        }

        private void plotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EegPlotForm form = new EegPlotForm();
            form.ShowDialog();
        }

        private void trainMotorImageryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataRecordForm form = new DataRecordForm();
            form.ShowDialog();
        }

        private void vEPControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VEPControlForm form = new VEPControlForm();
            form.ShowDialog();
        }

        private void recordNewSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            P300Form form = new P300Form();
            form.ShowDialog();
        }

        private void trainClassifierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClassifierTrainingForm form = new ClassifierTrainingForm();
            DialogResult result =  form.ShowDialog();
            if (result == DialogResult.OK)
            {
                p300Classifier = form.Classifier;
            }
        }

        private void runTestSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (p300Classifier == null)
            {
                MessageBox.Show("Сначана необходимо обучить классификатор");
                return;
            }

            P300Form form = new P300Form(TrialCount, p300Classifier);
            form.ShowDialog();
        }
    }
}
