﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using DataAcquisition.TransferObjects;

namespace tpu_bci
{
    class EegPlotter
    {
        //private variables
        private Graphics graphics;

        private int width;

        private int height;

        private float previousX = 0;

        private float[] previousY;

        //public properties
        public int Scale { get; set; }

        public int Duration { get; set; }

        public EegPlotter(Graphics graphics, int width, int height)
        {
            this.graphics = graphics;
            this.width = width;
            this.height = height;
        }

        public void Plot(EegSampleData eegSampleData)
        {
            if (previousY == null)
            {
                previousY = new float[eegSampleData.EegChannelsNumber];
            }

            int chanelHeight = height / eegSampleData.EegChannelsNumber;
            float deltaX = (float) width / (eegSampleData.SamplingRate * Duration);

            for (int i=0;i < eegSampleData.Data.Count; i++)
            { 
                float x = previousX + deltaX;

                //clean column
                graphics.FillRectangle(Brushes.White, previousX + 0.01f, 0, 2 * deltaX, height);

                float[] sample = eegSampleData.Data[i];
                for (int j = 0; j < sample.Length - eegSampleData.EventChannelsNumber; j++)
                {
                    float y = j * chanelHeight + chanelHeight / 2 - chanelHeight * (sample[j] / Scale) / 2;
                    graphics.DrawLine(Pens.Black, previousX, previousY[j], x, y);
                    previousY[j] = y;
                }

                if (previousX < width - deltaX)
                {
                    previousX = x;
                }
                else
                {
                    previousX = 0;
                }
                           
            }
        }
    }
}
