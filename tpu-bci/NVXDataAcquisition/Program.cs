﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NVXDataAcquisition
{
    class Program
    {
        static void Main(string[] args)
        {
            NVXAcquirer nvx = new NVXAcquirer();

            nvx.Connect();

          //  nvx.StartAcquisition();

            while (true)
            {
                if (nvx.HasData())
                {
                    byte[] data = nvx.GetData();
                    string hex = BitConverter.ToString(data);
                    Console.WriteLine(hex);
                }
            }
        }
    }
}
