﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NVXDataAcquisition
{
    interface IAcquirer
    {
        void StartAcquisition();

        void StopAcqusition();

        bool HasData();

        byte[] GetData();
    }
}
