﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.IO.Ports;
using USBHIDDRIVER;

namespace NVXDataAcquisition
{
    public class NVXAcquirer : IAcquirer
    {
        private const string Host = "localhost";

        private const int Port = 2700; //TODO: выяснить номер порта через снифер!

        private const int BuffetSize = 256;


        private Socket socket;

        private List<byte[]> data = new List<byte[]>();

        private USBInterface usb;


        public void Connect()
        {
            //SerialPort usb = new SerialPort(@"USB\VID_20F9&PID_0109\5&1795AA7&0&2");
            //usb.Open();
          //  USBHIDDRIVER.USBInterface usb = new USBInterface("vid_20F9");
         //   String[] list = usb.getDeviceList();
            usb = new USBInterface("vid_20F9", "pid_0109");
            usb.Connect();

            Console.WriteLine("Connected to usb.");     

          

            byte[] startCommand = new byte[] { 0x5a, 0xa5, 0x00, 0x02, 0x00, 0x00, 0xd5, 0x9f }; //TODO: здесь использовать команду, определенную через USB Port monitor 
            usb.write(startCommand);

            usb.enableUsbBufferEvent(new System.EventHandler(ProcessUsbData));
            Thread.Sleep(5);
            usb.startRead();  
        }

        private void ProcessUsbData(object o, EventArgs args)
        {
            Console.WriteLine(o);
        }


        public void StartAcquisition()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(Host, Port);

            Console.WriteLine("Connected.");

            //Запускает тред для приема данных
            Thread receiveThread = new Thread(new ThreadStart(Receive));
            receiveThread.Start();

            byte[] startCommand = new byte[] { 0x5a, 0xa5, 0x02, 0x00, 0x00, 0x00, 0xdd, 0x1c }; //TODO: здесь использовать команду, определенную через USB Port monitor 

            socket.Send(startCommand);

            Console.WriteLine("Command send");
        }

        public void Receive()
        {
            byte[] buffer = new byte[BuffetSize]; //TODO: надо понять, сколько байт читать
            while (true)
            {
                int received = socket.Receive(buffer);

                if (received > 0)
                {
                    //byte[] currentData =обрезать по received
                    if (isCommand(buffer))
                    {
                        ProcessCommand(buffer);
                    }
                    else
                    {
                        data.Add(buffer);
                    }
                }                
            }
        }

        public void ReceiveUsb()
        {
            byte[] buffer = new byte[BuffetSize]; //TODO: надо понять, сколько байт читать
            while (true)
            {
                int received = socket.Receive(buffer);
                if (received > 0)
                {
                    //byte[] currentData =обрезать по received
                    if (isCommand(buffer))
                    {
                        ProcessCommand(buffer);
                    }
                    else
                    {
                        data.Add(buffer);
                    }
                }
            }
        }

        private bool isCommand(byte[] data)
        {
            return true;
        }

        private void ProcessCommand(byte[] data)
        {

        }

        public void StopAcqusition()
        {
            throw new NotImplementedException();
        }

        public bool HasData()
        {
            return data.Count > 0;
        }

        public byte[] GetData()
        {
            if (HasData())
            {
                byte[] sample = data[0];
                data.RemoveAt(0);
                return sample;
            }
            else
            {
                throw new ArgumentException("Data buffer is empty");
            }
        }
    }
}
