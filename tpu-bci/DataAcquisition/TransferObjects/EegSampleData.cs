﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition.TransferObjects
{
    public class EegSampleData
    {
        public List<float[]> Data { get; set; }

        public int EegChannelsNumber { get; set; }

        public int EventChannelsNumber { get; set; }

        public int SamplingRate { get; set; }

        public long Timestamp { get; set; }

        public EegSampleData()
        {
            Data = new List<float[]>();
        }     

        public void Add(float[] sample)
        {
            Data.Add(sample);
        }

        public int AllChannelsNumber
        {
            get
            {
                return EegChannelsNumber + EventChannelsNumber;
            }
        }
      
    }
}
