﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition
{
    public class BasicInfo
    {
        //the oreder is important, because this information is stored in server message in such order

        public int Size { get; set; } //for version control, unused yet

        public int EegChannelsNumber { get; set; }

        public int EventChannelsNumber { get; set; }

        public int SampelsPerBlock { get; set; }

        public int SamplingRate { get; set; }

        public int DataSize { get; set; } // 2 for "short", 4 for "int" type of data

        public float Resolution { get; set; } // Resolution for LSB

        public static BasicInfo FromServerMessage(byte[] data)
        {
            BasicInfo info = new BasicInfo();

            info.Size = BitConverter.ToInt32(data, 0);
            info.EegChannelsNumber = BitConverter.ToInt32(data, 4);
            info.EventChannelsNumber = BitConverter.ToInt32(data, 8);
            info.SampelsPerBlock = BitConverter.ToInt32(data, 12);
            info.SamplingRate = BitConverter.ToInt32(data, 16);
            info.DataSize = BitConverter.ToInt32(data, 20);
            info.Resolution = BitConverter.ToSingle(data, 24);

            return info;
        }

        public int AllChannelsNumber
        {
            get
            {
                return EegChannelsNumber + EventChannelsNumber;
            }
        }
    }
}
