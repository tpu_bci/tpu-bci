﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition
{
    class RawData
    {
        public byte[] Data { get; set; }

        public long Timestamp { get; set; }
    }
}
