﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using DataAcquisition.TransferObjects;

namespace DataAcquisition
{
    public class FakeEegReceiver : IEegReceiver
    {
        private const ushort DefaultFrequency = 100;

        private const ushort DefaultChannels = 24; 

        private const float DefaultResolution = 0.49f; 

        private volatile bool isActive;

        private readonly ushort frequency;

        private readonly ushort channels; //Hz

        private readonly float resolution; // mV

        public ProcessEegData ProcessEegData { private get; set; }

        public BasicInfo BasicInfo { get; private set; }

        public FakeEegReceiver()
        {
            this.channels = DefaultChannels;
            this.frequency = DefaultFrequency;
            this.resolution = DefaultResolution;
        }

        public FakeEegReceiver(ushort channels, ushort frequency, float resolution)
        {
            this.frequency = frequency;
            this.channels = channels;
        }

        public void StartReceiving()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += DoWork;

            BasicInfo = new BasicInfo
            {
                EegChannelsNumber = channels,
                EventChannelsNumber = 0,
                SamplingRate = frequency,
                Resolution = resolution
            };

            worker.RunWorkerAsync();

            isActive = true;
        }

        private void DoWork(object sende, DoWorkEventArgs ergs)
        {
            Random random = new Random();
            while (isActive)
            {
                long timestamp = TimestampHelper.GetInstance().GetCurrentMilliseconds();

                Thread.Sleep(1000 / 4 );
                
                EegSampleData eeg = new EegSampleData
                {
                    EegChannelsNumber = this.channels,
                    EventChannelsNumber = 0,
                    Timestamp = timestamp
                };

                List<float[]> data = new List<float[]>();

                for (int i = 0; i < (frequency / 4); i++)
                {
                    byte[] buffer = new byte[channels];
                    random.NextBytes(buffer);
                    float[] sample = buffer.Select(o => (float)o).ToArray();
                    data.Add(sample);
                }

                eeg.Data = data;

                ProcessEegData(eeg);
            }
        }

        public void StopReceiving()
        {
            isActive = false;
        }
        
    }
}
