﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcquisition
{
    public interface IAcquirer
    {
        void Start(string serverName, int portNumber);
        void Stop();
    }
}
