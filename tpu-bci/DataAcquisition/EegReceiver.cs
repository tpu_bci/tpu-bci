﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DataAcquisition.TransferObjects;
using System.Threading;

namespace DataAcquisition
{    

    public class EegReceiver : IEegReceiver
    {
        private const int EmptyBufferDelayTime = 10; //ms

        private TcpAcquirer tcpAcquirer;

        public ProcessEegData ProcessEegData { private get;  set; }

        public BasicInfo BasicInfo { get; private set; }

        public EegReceiver()
        {
            IDataReceiver dataReceiver = new DataReceiver(this);
            tcpAcquirer = new TcpAcquirer(dataReceiver);
        }

        public void StartReceiving()
        {
            tcpAcquirer.Start(Connection.GetServerName(), Connection.GetPortNumber());

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += DoWork;    
            worker.RunWorkerAsync();
        }

        public void StopReceiving()
        {
            tcpAcquirer.Stop();
        }

        private void DoWork(object sende, DoWorkEventArgs ergs)
        {
            while (tcpAcquirer.IsActive)
            {
                if (tcpAcquirer.HasEegData)
                {
                    EegSampleData eegSampleData = tcpAcquirer.GetEegData();
                    ProcessEegData(eegSampleData);
                }
                else
                {
                    Thread.Sleep(EmptyBufferDelayTime);
                }
            }
        }       

        private class DataReceiver : DummyDataReceiver
        {
            private readonly EegReceiver receiver;

            public DataReceiver(EegReceiver receiver)
            {
                this.receiver = receiver;
            }

            public override void ProcessBasicInfo(BasicInfo basicInfo)
            {
                this.receiver.BasicInfo = basicInfo;
            }
        }
    }

}
