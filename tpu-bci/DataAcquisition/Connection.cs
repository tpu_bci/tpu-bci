﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcquisition
{
    public class Connection
    {
        private const string ServerName = "localhost";

        private const int PortNumber = 4000;

        public static string GetServerName()
        {
            return ServerName;
        }

        public static int GetPortNumber()
        {
            return PortNumber;
        }
    }
}
