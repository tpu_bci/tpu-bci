﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition
{
    public interface IDataReceiver
    {
        void ProcessBasicInfo(BasicInfo basicInfo);

        void StartAcquisition();

        void StopAcquisition();

        void StartImpedance();

        void Disconnected();
    }
}
