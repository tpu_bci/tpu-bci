﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition
{
    public class DummyDataReceiver : IDataReceiver
    {
        public virtual void ProcessBasicInfo(BasicInfo basicInfo)
        {
            
        }

        public virtual void StartAcquisition()
        {
            
        }

        public virtual void StopAcquisition()
        {
            
        }

        public virtual void StartImpedance()
        {
            
        }

        public virtual void Disconnected()
        {
            
        }
    }
}
