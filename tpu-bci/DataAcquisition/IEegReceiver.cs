﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAcquisition.TransferObjects;

namespace DataAcquisition
{
    public delegate void ProcessEegData(EegSampleData eegSampleData);

    public interface IEegReceiver
    {
        void StartReceiving();

        void StopReceiving();

        ProcessEegData ProcessEegData { set; }

        BasicInfo BasicInfo { get; }
    }
}
