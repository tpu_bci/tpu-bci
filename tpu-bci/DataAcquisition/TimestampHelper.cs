﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition
{
    public class TimestampHelper
    {
        private const int TicksPerMilliseconds = 10000;

        private static TimestampHelper instance;

        private long startTicks;

        private TimestampHelper() {}

        public static TimestampHelper GetInstance()
        {
            if (instance == null)
            {
                instance= new TimestampHelper();
                instance.startTicks = DateTime.Now.Ticks;
            }

            return instance;
        }

        public long GetCurrentMilliseconds()
        {
            long ticks = DateTime.Now.Ticks;
            long delta = ticks - startTicks;
            long milliseconds = delta/TicksPerMilliseconds;
            return milliseconds;
        }
    }
}
