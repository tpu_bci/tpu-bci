﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using DataAcquisition.Commands;
using DataAcquisition.TransferObjects;

namespace DataAcquisition
{
    public class TcpAcquirer : IAcquirer
    {
        private SocketAdaptor socketAdapter;

        public TcpAcquirer(IDataReceiver dataReceiver)
        {
            socketAdapter = new SocketAdaptor(dataReceiver);
        }

        public void Start(string severName, int port)
        {                   
            socketAdapter.Connect(severName, port);
            socketAdapter.IsActive = true;   
            socketAdapter.StartReceiving();           
        }      

        public void Stop()
        {
            if (socketAdapter.IsActive)
            {
                socketAdapter.StopReceiving();
            }
        }

        public bool IsActive
        {
            get
            {
                return socketAdapter.IsActive;
            }
        }

        public bool HasEegData
        {
            get
            {
                return socketAdapter.HasData;
            }
        }

        public EegSampleData GetEegData()
        {
            return socketAdapter.GetData();
        }
    }   
}
