﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition.Commands
{
    // "CTRL" packet control codes
    enum ControlCode
    {
        GeneralControlCode = 1,
        ServerControlCode, 
        ClientControlCode       
    }
}
