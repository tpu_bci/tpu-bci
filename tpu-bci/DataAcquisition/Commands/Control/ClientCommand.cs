﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition.Commands
{
    enum ClientCommand
    {
        RequestEdfHeader = 1,
        RequestAstFile,
        RequestStartData,
        RequestStopData, 
        RequestBasicInfo 
    }
}
