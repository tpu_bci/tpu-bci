﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition.Commands
{
    enum ServerCommand
    {
        StartAcquisition = 1,
        StopAcquisition,
        StartImpedance,
        ChangeSetup, 
        DCCorrection
    }
}
