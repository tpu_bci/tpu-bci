﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition.Commands
{
    enum DataCode
    {
        DataType_InfoBlock = 1, 
        DataType_EegData
    }
}
