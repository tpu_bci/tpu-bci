﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAcquisition.Commands
{
    enum InfoBlockType
    {
        InfoType_Version = 1, 
        InfoType_EdfHeader, 
        InfoType_BasicInfo
    }
}
